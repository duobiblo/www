export const send = async (url, body, type='POST', mimeType = 'text/json') => {
    const xhr = new XMLHttpRequest()
    xhr.open(type, url)
    xhr.overrideMimeType(mimeType)
    await new Promise((resolve) => {
        xhr.onload = resolve
        xhr.send(body)
    })
    return { status: xhr.status, response: xhr.response }
}
export const post = async (url, body) => send(url, body)

