import { writable } from 'svelte/store'

export const currentSchema = writable('mutedTones')

export const colorSchemas = {
    mutedTones: { 
        color1: '#C0B283', /* pale gold */
        color2: '#DCD0C0', /* silk */
        color3: '#F4F4F4', /* paer */
        color4: '#373737', /* charcoal */
    },
    mutedMinimal: { 
        color1: '#96858F', /* dusty */
        color2: '#6D7993', /* lavendar */
        color3: '#9099A2', /* overcast */
        color4: '#D5D5D5', /* paper */
        color5: '#373737', /* charcoal */
    },
    boldAndPunch: { 
        color1: '#E24E42', /* papaya */
        color2: '#E9B000', /* mustard */
        color3: '#EB6E80', /* blush */
        color4: '#008F95', /* aqua */
    },
    summer: { 
        color1: '#286DA8', /* summer sky */
        color2: '#CD5360', /* warm peach */
        color3: '#B37D4E', /* field */
        color4: '#438496', /* breeze */
    },
    freshBlues: { 
        color1: '#77C9D4', /* feather */
        color2: '#57BC90', /* marine */
        color3: '#015249', /* forest */
        color4: '#A5A5AF', /* sleek grey */
    },
    unexpected: { 
        color1: '#6E3667', /* violet */
        color2: '#88D317', /* electrc lime */
        color3: '#1A0315', /* deep plum */
        color4: '#535353', /* shadow */
    },
    earthy: { 
        color1: '#945D60', /* terracota */
        color2: '#626E60', /* herb */
        color3: '#AF473C', /* chilli */
        color4: '#3C3C3C', /* charcoal */
        color4: '#D5D5D5', /* paper for background */
    },
}
