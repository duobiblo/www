import bibleInfos from '../lib/bibleInfos.js'

export { bibleInfos }

export const DUOBIBLO_BAK = 'https://nehemia.duobiblo.com'
// export const DUOBIBLO_BAK = 'http://localhost:3020'

export const DEFAULTS = {
    PROJECT_NAME: 'Duobiblo',
}

export const availableUILanguages = [
    [ 'en', 'English', 'English' ],
    [ 'es', 'Spanish', 'Español' ],
    [ 'de', 'German', 'Deutsch' ],
]

